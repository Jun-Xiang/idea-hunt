import 'dart:math';
import 'auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AddProjectsPage extends StatefulWidget {
  AddProjectsPage({this.auth});
  final BaseAuth auth;
  @override
  _AddProjectsPageState createState() => _AddProjectsPageState();
}

class _AddProjectsPageState extends State<AddProjectsPage> {
  final String backPath = 'assets/icons/back.svg';
  bool tapped = false;
  bool tapped1 = false;
  bool content = false;
  var selected = [];
  var tags = ['UI/UX', 'Web Development', 'App Development', 'Logo Design'];
  var selectedList = [];
  var ideatitle;
  var ideadesc;
  var userid;

  @override
  void initState() {
    super.initState();
    widget.auth.currentUser().then((String getuserid) {
      setState(() {
        userid = getuserid;
      });
    });
  }

  selectTags() {
    setState(() {
      tapped = false;
      tapped1 = false;
    });
    selected.isEmpty
        ? tags.forEach((tag) {
            selected.add({tag: false});
          })
        : null;
    var tagsSelection = showModalBottomSheet(
        context: context,
        builder: (BuildContext context) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) =>
                  Container(
                height: MediaQuery.of(context).size.height * 0.5,
                child: ListView.builder(
                    itemCount: tags.length,
                    itemBuilder: (context, i) => ListTile(
                          contentPadding: EdgeInsets.all(10.0),
                          title: Text(
                            tags[i],
                            style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'Neue',
                                fontWeight: FontWeight.w300),
                          ),
                          trailing: selected[i][tags[i]]
                              ? Icon(Icons.check_box)
                              : Icon(Icons.check_box_outline_blank),
                          onTap: () {
                            setState(() {
                              selected[i][tags[i]] = !selected[i][tags[i]];
                            });
                          },
                        )),
              ),
            ));
    getList() {
      selectedList = [];
      setState(() {
        selected.forEach((selection) {
          selection[tags[selected.indexOf(selection)]] == true
              ? selectedList.add(tags[selected.indexOf(selection)])
              : null;
        });
      });
    }

    tagsSelection.whenComplete(getList);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.only(left: 10, bottom: 15.0),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: SizedBox(
                          height: 40,
                          width: 40,
                          child: SvgPicture.asset(backPath)),
                    ),
                  ),
                  Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 15.0),
                          child: Text(
                            'New Idea',
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Neue',
                                fontSize: 25),
                          ))),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: 40,
                    child: FlatButton(
                      onPressed: content
                          ? () {
                              createNewIdea(ideatitle, ideadesc, selectedList,
                                  userid, context);
                            }
                          : null,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text('ADD'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
              setState(() {
                tapped = false;
                tapped1 = false;
              });
            },
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
              child: Form(
                child: Column(
                  children: <Widget>[
                    TextField(
                      onChanged: (input) {
                        ideatitle = input;
                        input != ''
                            ? setState(() {
                                content = true;
                              })
                            : setState(() {
                                content = false;
                              });
                      },
                      onTap: () {
                        setState(() {
                          tapped = true;
                          tapped1 = false;
                        });
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(255, 179, 77, 1))),
                          focusColor: Colors.black,
                          labelText: 'Idea Title',
                          labelStyle: TextStyle(
                            color: tapped ? Colors.black : Colors.grey,
                          )),
                    ),
                    Row(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Tags'),
                          onPressed: selectTags,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: MediaQuery.of(context).size.height * 0.1,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: selectedList.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: EdgeInsets.only(right: 6),
                                  child: Chip(
                                    label: Text(selectedList[index],
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12)),
                                    backgroundColor:
                                        Color.fromRGBO(0, 255, 156, 1),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(6.0))),
                                  ),
                                );
                              }),
                        )
                      ],
                    ),
                    TextField(
                        onChanged: (input) {
                          ideadesc = input;
                        },
                        onTap: () {
                          setState(() {
                            tapped = false;
                            tapped1 = true;
                          });
                        },
                        minLines: 10,
                        maxLines: 11,
                        decoration: InputDecoration(
                            alignLabelWithHint: true,
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.transparent)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromRGBO(255, 179, 77, 1))),
                            focusColor: Colors.black,
                            labelText: 'Description',
                            labelStyle: TextStyle(
                              color: tapped1 ? Colors.black : Colors.grey,
                            ))),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

String generateID() {
  var rng = new Random();
  var rngnum = "p";
  for (var i = 0; i < 6; i++) {
    rngnum += rng.nextInt(10).toString();
  }
  return rngnum;
}

void createNewIdea(title, desc, tags, userid, context) {
  var generatedID = generateID();
  FirebaseDatabase.instance
      .reference()
      .child('projectdatas')
      .once()
      .then((DataSnapshot snapshot) {
    if (snapshot.value != null) {
      if (snapshot.value.length == 0) {
        var newRecordRef =
            FirebaseDatabase.instance.reference().child('projectdatas/0');
        newRecordRef.update({
          'id': generatedID,
          'comments': '',
          'desc': desc,
          'duedate': '',
          'host': userid,
          'involved': '',
          'name': title,
          'tags': tags,
          'upvotes': '',
        }).then((future) {
          FirebaseDatabase.instance
              .reference()
              .child('userdatas/' + userid + '/ideas')
              .once()
              .then((DataSnapshot snapshot1) {
            if (snapshot1.value != '') {
              var synclist = new List<dynamic>.from(snapshot1.value);
              synclist.add(generatedID);
              var updateIdeasRef = FirebaseDatabase.instance
                  .reference()
                  .child('userdatas/' + userid + '/ideas');
              updateIdeasRef.set(synclist).then((future){
                Navigator.pop(context);
              });
            } else {
              List synclist = [];
              synclist.add(generatedID);
              var updateIdeasRef = FirebaseDatabase.instance
                  .reference()
                  .child('userdatas/' + userid + '/ideas');
              updateIdeasRef.set(synclist).then((future){
                Navigator.pop(context);
              });
            }
          });
        });
      } else {
        for (int o = 0; o <= snapshot.value.length - 1; o++) {
          if (generatedID == snapshot.value[o]['projectid'].toString()) {
            createNewIdea(title, desc, tags, userid, context);
          } else {
            if (o == snapshot.value.length - 1) {
              if (snapshot.value.length == 0) {
                var newRecordRef = FirebaseDatabase.instance
                    .reference()
                    .child('projectdatas/0');
                newRecordRef.update({
                  'id': generatedID,
                  'comments': '',
                  'desc': desc,
                  'duedate': '',
                  'host': userid,
                  'involved': '',
                  'name': title,
                  'tags': tags,
                  'upvotes': '',
                }).then((future) {
                  FirebaseDatabase.instance
                      .reference()
                      .child('userdatas/' + userid + '/ideas')
                      .once()
                      .then((DataSnapshot snapshot1) {
                    if (snapshot1.value != '') {
                      var synclist = new List<dynamic>.from(snapshot1.value);
                      synclist.add(generatedID);
                      var updateIdeasRef = FirebaseDatabase.instance
                          .reference()
                          .child('userdatas/' + userid + '/ideas');
                      updateIdeasRef.set(synclist).then((future){
                        Navigator.pop(context);
                      });
                    } else {
                      List synclist = [];
                      synclist.add(generatedID);
                      var updateIdeasRef = FirebaseDatabase.instance
                          .reference()
                          .child('userdatas/' + userid + '/ideas');
                      updateIdeasRef.set(synclist).then((future){
                        Navigator.pop(context);
                      });
                    }
                  });
                });
              } else {
                var newRecordRef = FirebaseDatabase.instance
                    .reference()
                    .child('projectdatas/' + snapshot.value.length.toString());
                newRecordRef.set({
                  'id': generatedID,
                  'comments': '',
                  'desc': desc,
                  'duedate': '',
                  'host': userid,
                  'involved': '',
                  'name': title,
                  'tags': tags,
                  'upvotes': '',
                }).then((future) {
                  FirebaseDatabase.instance
                      .reference()
                      .child('userdatas/' + userid + '/ideas')
                      .once()
                      .then((DataSnapshot snapshot1) {
                    if (snapshot1.value != '') {
                      var synclist = new List<dynamic>.from(snapshot1.value);
                      synclist.add(generatedID);
                      var updateIdeasRef = FirebaseDatabase.instance
                          .reference()
                          .child('userdatas/' + userid + '/ideas');
                      updateIdeasRef.set(synclist).then((future){
                        Navigator.pop(context);
                      });
                    } else {
                      List synclist = [];
                      synclist.add(generatedID);
                      var updateIdeasRef = FirebaseDatabase.instance
                          .reference()
                          .child('userdatas/' + userid + '/ideas');
                      updateIdeasRef.set(synclist).then((future){
                        Navigator.pop(context);
                      });
                    }
                  });
                });
              }
            }
          }
        }
      }
    } else {
      var newRecordRef =
          FirebaseDatabase.instance.reference().child('projectdatas/0');
      newRecordRef.update({
        'id': generatedID,
        'comments': '',
        'desc': desc,
        'duedate': '',
        'host': userid,
        'involved': '',
        'name': title,
        'tags': tags,
        'upvotes': '',
      }).then((future) {
        FirebaseDatabase.instance
            .reference()
            .child('userdatas/' + userid + '/ideas')
            .once()
            .then((DataSnapshot snapshot1) {
          if (snapshot1.value != '') {
            var synclist = new List<dynamic>.from(snapshot1.value);
            synclist.add(generatedID);
            var updateIdeasRef = FirebaseDatabase.instance
                .reference()
                .child('userdatas/' + userid + '/ideas');
            updateIdeasRef.set(synclist).then((future){
              Navigator.pop(context);
            });
          } else {
            List synclist = [];
            synclist.add(generatedID);
            var updateIdeasRef = FirebaseDatabase.instance
                .reference()
                .child('userdatas/' + userid + '/ideas');
            updateIdeasRef.set(synclist).then((future){
              Navigator.pop(context);
            });
          }
        });
      });
    }
  });
}
