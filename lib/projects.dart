import 'package:flutter/material.dart';
import 'projectsView.dart';
import 'auth.dart';

class Projects extends StatefulWidget {
  final usrid;
  Projects({this.usrid});
  @override
  _ProjectsState createState() => _ProjectsState();
}

class _ProjectsState extends State<Projects> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ProjectsTaken(userid: widget.usrid),
    );
  }
}

class ProjectsTaken extends StatefulWidget {
  final userid;
  ProjectsTaken({this.userid});
  @override
  _ProjectsTakenState createState() => _ProjectsTakenState();
}

class _ProjectsTakenState extends State<ProjectsTaken> {
  List projectId = [];

  var counter = 0;

  List<Widget> preview = [];

  Widget view(context, id) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.95,
        margin: EdgeInsets.only(
            top: 10,
            left: MediaQuery.of(context).size.width * 0.05,
            right: MediaQuery.of(context).size.width * 0.05),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.grey[200],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 4.0, left: 15.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 250,
                    child: StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var chipIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                chipIndex = i;
                              }
                            }
                            if (projectId.isEmpty == false) {
                              if (snap.data.snapshot
                                  .value[chipIndex]['tags'] != null) {
                                return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: snap.data.snapshot
                                        .value[chipIndex]['tags'].length,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: EdgeInsets.only(right: 6),
                                        child: Chip(
                                          label: Text(
                                              snap.data.snapshot.value[chipIndex]
                                              ['tags'][index],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12)),
                                          backgroundColor:
                                          Color.fromRGBO(0, 255, 156, 1),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(6.0))),
                                        ),
                                      );
                                    });
                              } else {
                                return Padding(padding: EdgeInsets.only(top: 10));
                              }
                            } else {
                              return Padding(padding: EdgeInsets.only(top: 10));
                            }
                          } else {
                            return Padding(padding: EdgeInsets.only(top: 10));
                          }
                        }),
                  ),
                  StreamBuilder(
                      stream: databaseReference.onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          var picIndex;
                          var picurl;
                          for (var i = 0;
                              i <
                                  snap.data.snapshot.value['projectdatas']
                                      .length;
                              i++) {
                            if (snap.data.snapshot.value['projectdatas'][i]
                                    ['id'] ==
                                id) {
                              picIndex = i;
                            }
                          }
                          if (snap.data.snapshot.value['userdatas'][
                                  snap.data.snapshot.value['projectdatas']
                                      [picIndex]['host']]['profilepicurl'] !=
                              '') {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(snap
                                          .data.snapshot.value['userdatas'][snap
                                              .data
                                              .snapshot
                                              .value['projectdatas'][picIndex]
                                          ['host']]['profilepicurl'])),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0))),
                            );
                          } else {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/profpic.png')),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0))),
                            );
                          }
                        } else {
                          return Container(
                            margin: EdgeInsets.only(right: 15),
                            height: 27,
                            width: 27,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/profpic.png')),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0))),
                          );
                        }
                      })
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var ideaIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                ideaIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[ideaIndex]['name'],
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          } else {
                            return Text('',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var descIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                descIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[descIndex]['desc'],
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          } else {
                            return Text('',
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 18, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 22),
                      child: StreamBuilder(
                          stream:
                              databaseReference.child('projectdatas/').onValue,
                          builder: (context, snap) {
                            if (snap.hasData &&
                                !snap.hasError &&
                                snap.data.snapshot.value != null) {
                              var idIndex;
                              for (var i = 0;
                                  i < snap.data.snapshot.value.length;
                                  i++) {
                                if (snap.data.snapshot.value[i]['id'] == id) {
                                  idIndex = i;
                                }
                              }
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return ProjectsView(
                                      id: idIndex,
                                      userid: widget.userid,
                                      projectid: id,
                                    );
                                  }));
                                },
                              );
                            } else {
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: null,
                              );
                            }
                          }),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  buildView(BuildContext context) {
    preview = [];
    for (var i = 0; i <= projectId.length - 1; i++) {
      preview.add(view(context, projectId[i]));
    }
    return preview.toList();
  }

  var doneGet = false;

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, setState) => StreamBuilder(
          stream: databaseReference.child('userdatas/').onValue,
          builder: (context, snap) {
            if (snap.hasData &&
                !snap.hasError &&
                snap.data.snapshot.value != null) {
              Auth().currentUser().then((String returnedID) {
                if (doneGet == false) {
                  for (var i = 0;
                      i <=
                          snap.data.snapshot.value[returnedID]['ideas'].length -
                              1;
                      i++) {
                    setState(() {
                      projectId.add(
                          snap.data.snapshot.value[returnedID]['ideas'][i]);
                    });
                  }
                }
              }).then((future) {
                doneGet = true;
              });
              return Column(
                  mainAxisSize: MainAxisSize.min, children: buildView(context));
            } else {
              return Column(
                  mainAxisSize: MainAxisSize.min, children: buildView(context));
            }
          }),
    );
  }
}
