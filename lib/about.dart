import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_svg/flutter_svg.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  final String backPath = 'assets/icons/back.svg';
  WebViewController _controller;
  final url = 'https://gitlab.com/Jun-Xiang/idea-hunt/blob/master/README.md';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text('About', style: TextStyle(color: Colors.black, fontSize: 15.0),),
        leading: FlatButton(
          child: Icon(Icons.close),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
//        bottom: PreferredSize(
//          preferredSize: Size.fromHeight(20.0),
//          child: Container(
//            padding: EdgeInsets.only(left: 10, bottom: 15.0),
//            child: Row(
//              children: <Widget>[
//                IconButton(
//                  onPressed: () {
//                    Navigator.pop(context);
//                  },
//                  icon: SizedBox(
//                      height: 40,
//                      width: 40,
//                      child: SvgPicture.asset(backPath)),
//                ),
//              ],
//            ),
//          ),
//        ),
      ),
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height ,
          child: WebView(
            key: UniqueKey(),
            initialUrl: url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController c){
              _controller = c;
            },
          ),
        ),
      ),
    );
  }
}
