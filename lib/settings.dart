import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dashboard.dart';
import 'auth.dart';

class Settings extends StatefulWidget {
  final logOut;
  final BaseAuth auth;
  Settings({this.auth, this.logOut});
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final String optionsWebDevPath = 'assets/icons/options_web_dev.svg';
  final String optionsUIUXPath = 'assets/icons/options_uiux.svg';
  final String optionsLogoDesignPath = 'assets/icons/options_logo_design.svg';
  final String optionsAppDevPath = 'assets/icons/options_app_dev.svg';
  List interests = [];
  List view = [];
  var counter = 0;
  void signOut() async {
    try {
      await widget.auth.signOut();
      Navigator.pop(context);
      widget.logOut();
    } catch (e) {}
  }

  var userid;
  @override
  void initState() {
    super.initState();
    widget.auth.currentUser().then((String getuserid) {
      setState(() {
        userid = getuserid;
      });
    });
  }
  getInterestView() {
    counter == 0
        ? interests.forEach((interest) {
            counter++;
            switch (interest) {
              case 'ui/ux-design':
                view.add(InterestView(
                  img: optionsUIUXPath,
                  title: 'UI/UX Design',
                ));
                break;
              case 'web-development':
                view.add(InterestView(
                  img: optionsWebDevPath,
                  title: 'Web Development',
                ));
                break;
              case 'logo-design':
                view.add(InterestView(
                  img: optionsLogoDesignPath,
                  title: 'Logo Design',
                ));
                break;
              case 'app-development':
                view.add(InterestView(
                  img: optionsAppDevPath,
                  title: 'App Development',
                ));
                break;
            }
            interests.indexOf(interest) == interests.length - 1
                ? view.add(AddInterest(userid: userid,))
                : null;
          })
        : null;
  }

  handleClick() {}
  final String backPath = 'assets/icons/back.svg';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.only(left: 10, bottom: 15.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: SizedBox(
                        height: 40,
                        width: 40,
                        child: SvgPicture.asset(backPath)),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.23 + 70,
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      SettingsProfilePic(userid: userid, auth: Auth()),
                      UserName(
                        userid: userid,
                        auth: Auth(),
                      ),
                      UserID(
                        userid: userid,
                        auth: Auth(),
                        edit: true,
                      ),
                    ],
                  )),
                ),
                SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey.withOpacity(0.5)),
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  StreamBuilder(
                                      stream: FirebaseDatabase.instance
                                          .reference()
                                          .child('userdatas/')
                                          .onValue,
                                      builder: (context, snap) {
                                        if (snap.hasData &&
                                            !snap.hasError &&
                                            snap.data.snapshot.value != null) {
                                          if (snap.data.snapshot.value[userid]
                                                  ['ideas'] ==
                                              '') {
                                            return Text(
                                              '0',
                                              style: TextStyle(
                                                  fontFamily: 'Neue',
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 30.0),
                                            );
                                          } else {
                                            return Text(
                                              snap.data.snapshot
                                                  .value[userid]['ideas'].length
                                                  .toString(),
                                              style: TextStyle(
                                                  fontFamily: 'Neue',
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 30.0),
                                            );
                                          }
                                        } else {
                                          return Text(
                                            '0',
                                            style: TextStyle(
                                                fontFamily: 'Neue',
                                                fontWeight: FontWeight.w700,
                                                fontSize: 30.0),
                                          );
                                        }
                                      }),
                                  Text(
                                    'ideas',
                                    style: TextStyle(
                                        fontFamily: 'Neue',
                                        fontWeight: FontWeight.w300,
                                        fontSize: 15.0),
                                  ),
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  StreamBuilder(
                                      stream: FirebaseDatabase.instance
                                          .reference()
                                          .child('userdatas/')
                                          .onValue,
                                      builder: (context, snap) {
                                        if (snap.hasData &&
                                            !snap.hasError &&
                                            snap.data.snapshot.value != null) {
                                          if (snap.data.snapshot.value[userid]
                                                  ['involved'] ==
                                              '') {
                                            return Text(
                                              '0',
                                              style: TextStyle(
                                                  fontFamily: 'Neue',
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 30.0),
                                            );
                                          } else {
                                            return Text(
                                              snap
                                                  .data
                                                  .snapshot
                                                  .value[userid]['involved']
                                                  .length
                                                  .toString(),
                                              style: TextStyle(
                                                  fontFamily: 'Neue',
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 30.0),
                                            );
                                          }
                                        } else {
                                          return Text(
                                            '0',
                                            style: TextStyle(
                                                fontFamily: 'Neue',
                                                fontWeight: FontWeight.w700,
                                                fontSize: 30.0),
                                          );
                                        }
                                      }),
                                  Text(
                                    'involved',
                                    style: TextStyle(
                                        fontFamily: 'Neue',
                                        fontWeight: FontWeight.w300,
                                        fontSize: 15.0),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.1,
                                MediaQuery.of(context).size.width * 0.1,
                                MediaQuery.of(context).size.width * 0.1,
                                0),
                            child: Text(
                              'My Interests',
                              style: TextStyle(
                                  fontFamily: 'Neue',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300),
                            ),
                          )),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: MediaQuery.of(context).size.height * 0.3,
                        child: StreamBuilder(
                            stream: FirebaseDatabase.instance
                                .reference()
                                .child('userdatas/')
                                .onValue,
                            builder: (context, snap) {
                              if (snap.hasData &&
                                  !snap.hasError &&
                                  snap.data.snapshot.value != null) {
                                for (int i = 0;
                                    i <=
                                        snap
                                                .data
                                                .snapshot
                                                .value[userid]['interests']
                                                .length -
                                            1;
                                    i++) {
                                  interests.add(snap.data.snapshot.value[userid]
                                      ['interests'][i]);
                                  i ==
                                          snap
                                                  .data
                                                  .snapshot
                                                  .value[userid]['interests']
                                                  .length -
                                              1
                                      ? getInterestView()
                                      : null;
                                }
                                return ListView.builder(
                                    itemCount: view.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (BuildContext context, i) {
                                      return view[i];
                                    });
                              } else {
                                return Padding(
                                    padding: EdgeInsets.only(top: 5));
                              }
                            }),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.04,
                            bottom: MediaQuery.of(context).size.height * 0.04),
                        child: ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          minWidth: MediaQuery.of(context).size.width * 0.65,
                          height: MediaQuery.of(context).size.height * 0.09,
                          buttonColor: Color.fromRGBO(255, 179, 77, 1),
                          child: RaisedButton(
                            onPressed: (){
                              Navigator.pushNamed(context, '/about');
                            },
                            child: Text(
                              'ABOUT',
                              style: TextStyle(
                                  color: Colors.white,
                                  letterSpacing: 5.0,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Neue'),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.04,
                            bottom: MediaQuery.of(context).size.height * 0.04),
                        child: ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          minWidth: MediaQuery.of(context).size.width * 0.65,
                          height: MediaQuery.of(context).size.height * 0.09,
                          buttonColor: Color.fromRGBO(255, 179, 77, 1),
                          child: RaisedButton(
                            onPressed: signOut,
                            child: Text(
                              'LOG OUT',
                              style: TextStyle(
                                  color: Colors.white,
                                  letterSpacing: 5.0,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'Neue'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SettingsProfilePic extends StatefulWidget {
  final BaseAuth auth;
  String userid;
  SettingsProfilePic({this.auth, this.userid});
  @override
  _SettingsProfilePicState createState() => _SettingsProfilePicState();
}

class _SettingsProfilePicState extends State<SettingsProfilePic> {
  var url;
  final formController = TextEditingController();

  @override
  dispose() {
    formController.dispose();
    super.dispose();
  }

  handleClick() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              titlePadding: EdgeInsets.all(10),
              title: ProfilePic(
                auth: Auth(),
                setWH: 0.1,
                setMargin: 0,
                color: Colors.white,
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextField(
                    controller: formController,
                    decoration:
                        InputDecoration(hintText: 'New Profile Picture URL'),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.05),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: ButtonTheme(
                            height: MediaQuery.of(context).size.height * 0.08,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            buttonColor: Color.fromRGBO(255, 179, 77, 1),
                            child: RaisedButton(
                              child: Text(
                                'SAVE',
                                style: TextStyle(
                                  letterSpacing: 5.0,
                                  fontSize: 13.0,
                                  fontFamily: 'Neue',
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                                url = formController.text;
                                FirebaseDatabase.instance
                                    .reference()
                                    .child('userdatas/' + widget.userid)
                                    .update({
                                  'profilepicurl': url,
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return ProfilePic(
      auth: Auth(),
      handlePress: () => handleClick(),
      setWH: 0.18,
      setMargin: 0,
    );
  }
}

class InterestView extends StatelessWidget {
  final img;
  final title;
  InterestView({this.img, this.title});
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: InkWell(
        onTap: () {},
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.03,
                  0,
                  MediaQuery.of(context).size.width * 0.03,
                  0),
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.width * 0.03,
                  bottom: MediaQuery.of(context).size.width * 0.03),
              width: MediaQuery.of(context).size.width * 0.4,
              height: MediaQuery.of(context).size.height * 0.2,
              child: SvgPicture.asset(img),
            ),
            Text(title),
          ],
        ),
      ),
    );
  }
}

class AddInterest extends StatefulWidget {
  final userid;
  AddInterest({this.userid});
  @override
  _AddInterestState createState() => _AddInterestState();
}

class _AddInterestState extends State<AddInterest> {
  final String addPath = 'assets/icons/editBlack.svg';
  void _alertWrongEmailAndPassword(error) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error !'),
            content: Text(error),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  void add() {
    bool userSubmitted = false;
    List selected = [
      {"option": "web-development", "selected": false, "color": Colors.white},
      {"option": "app-development", "selected": false, "color": Colors.white},
      {"option": "logo-design", "selected": false, "color": Colors.white},
      {"option": "ui/ux-design", "selected": false, "color": Colors.white}
    ];

    final String optionsWebDevPath = 'assets/icons/options_web_dev.svg';
    final String optionsUIUXPath = 'assets/icons/options_uiux.svg';
    final String optionsLogoDesignPath = 'assets/icons/options_logo_design.svg';
    final String optionsAppDevPath = 'assets/icons/options_app_dev.svg';
    var size = MediaQuery.of(context).size;
    var itemHeight = size.width * 0.4;
    var itemWidth = size.width * 0.3;

    void resetForm() async {
      if (userSubmitted == true) {
        return;
      }
      setState(() {
        selected.forEach((selection) {
          selection['selected'] = false;
          selection['color'] = Colors.white;
        });
      });
    }

    void submitForm(context) {
      List optionsSelected = [];
      selected.forEach((selection) {
        if (selection['selected'] == true) {
          optionsSelected.add(selection['option']);
        }
      });
      if (optionsSelected.length >= 1) {
        FirebaseDatabase.instance.reference().child('userdatas/' + widget.userid).update({
          'interests': optionsSelected,
        }).then((future) {
          Navigator.pop(context);
        });
        setState(() {
          userSubmitted = true;
        });
      } else {
        _alertWrongEmailAndPassword('Please select at least one!');
      }
    }

    var bottomsheet = showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            tapCard(index) {
              setState(() {
                selected[index]['selected'] = !(selected[index]['selected']);
                selected[index]['color'] =
                    selected[index]['selected'] ? Colors.grey : Colors.white;
              });
            }

            return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.9,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 179, 77, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(
                            top: 40,
                            bottom: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          'User Interest',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(fontFamily: 'Neue', fontSize: 25.0),
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01,
                          bottom: 30),
                      child: Text(
                        'Pick at least one from below',
                        style:
                            TextStyle(fontSize: 15.0, fontFamily: 'Neue Light'),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: GridView.count(
                        childAspectRatio: (itemWidth / itemHeight),
                        crossAxisCount: 2,
                        children: <Widget>[
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('Web Development'),
                            color: selected[0]['color'],
                            child: InkWell(
                              onTap: () => tapCard(0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsWebDevPath),
                                  ),
                                  Text('Web Development'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('App Development'),
                            color: selected[1]['color'],
                            child: InkWell(
                              focusColor: Colors.blueAccent,
                              onTap: () => tapCard(1),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsAppDevPath),
                                  ),
                                  Text('App Development'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('Logo Design'),
                            color: selected[2]['color'],
                            child: InkWell(
                              onTap: () => tapCard(2),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child:
                                        SvgPicture.asset(optionsLogoDesignPath),
                                  ),
                                  Text('Logo Design'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('UI/UX Design'),
                            color: selected[3]['color'],
                            child: InkWell(
                              onTap: () => tapCard(3),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsUIUXPath),
                                  ),
                                  Text('UI/UX Design'),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 30),
                      child: ButtonTheme(
                        height: MediaQuery.of(context).size.height * 0.09,
                        minWidth: MediaQuery.of(context).size.width * 0.7,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        buttonColor: Colors.white,
                        child: RaisedButton(
                          child: Text(
                            'DONE!',
                            style: TextStyle(
                                letterSpacing: 5.0,
                                fontFamily: 'Neue',
                                fontSize: 15.0,
                                color: Color.fromRGBO(255, 179, 77, 1)),
                          ),
                          onPressed: () => submitForm(context),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
        });
    bottomsheet.whenComplete(resetForm);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: InkWell(
        onTap: add,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.4,
          height: MediaQuery.of(context).size.height * 0.2,
          child: Center(
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.1,
                  height: MediaQuery.of(context).size.width * 0.1,
                  child: SvgPicture.asset(addPath))),
        ),
      ),
    );
  }
}

class UserName extends StatefulWidget {
  final BaseAuth auth;
  String userid;
  UserName({this.auth, this.userid});
  @override
  _UserNameState createState() => _UserNameState();
}

class _UserNameState extends State<UserName> {
  var fetchedUsername;
  var newUsername;
  final _formKey = GlobalKey<FormState>();
  bool validateForm() {
    var form = _formKey.currentState;
    form.save();
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  submitForm() {
    FirebaseDatabase.instance
        .reference()
        .child('userdatas/' + widget.userid)
        .update({
      'name': newUsername,
    });
  }

  inputDialog(username) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              title: Text(
                username,
                textAlign: TextAlign.center,
                style:
                    TextStyle(fontFamily: 'Neue', fontWeight: FontWeight.w700),
              ),
              content: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      onSaved: (input) => newUsername = input,
                      validator: (input) {
                        return input == null || input == ''
                            ? 'Input cannot be empty'
                            : null;
                      },
                      decoration: InputDecoration(hintText: 'Change Name'),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.05),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              height: MediaQuery.of(context).size.height * 0.08,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              buttonColor: Color.fromRGBO(255, 179, 77, 1),
                              child: RaisedButton(
                                child: Text(
                                  'SAVE',
                                  style: TextStyle(
                                    letterSpacing: 5.0,
                                    fontSize: 13.0,
                                    fontFamily: 'Neue',
                                    color: Colors.white,
                                  ),
                                ),
                                onPressed: () {
                                  if (validateForm()) {
                                    Navigator.pop(context);
                                    submitForm();
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 10),
        child: StreamBuilder(
            stream: FirebaseDatabase.instance
                .reference()
                .child('userdatas/')
                .onValue,
            builder: (context, snap) {
              if (snap.hasData &&
                  !snap.hasError &&
                  snap.data.snapshot.value != null) {
                fetchedUsername =
                    snap.data.snapshot.value[widget.userid]['name'];
                return FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      inputDialog(fetchedUsername);
                    },
                    child: Text(
                      snap.data.snapshot.value[widget.userid]['name'],
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Neue'),
                    ));
              } else {
                fetchedUsername = '';
                return FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    child: Text(''));
              }
            }),
      ),
    );
  }
}

class UserID extends StatefulWidget {
  final BaseAuth auth;
  final edit;
  String userid;
  UserID({this.auth, this.userid, this.edit});

  @override
  _UserIDState createState() => _UserIDState();
}

class _UserIDState extends State<UserID> {
  var fetchedUserid;
  var newid;
  final _formKey = GlobalKey<FormState>();
  bool validateForm() {
    var form = _formKey.currentState;
    form.save();
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  submitForm() {
    FirebaseDatabase.instance
        .reference()
        .child('userdatas/' + widget.userid)
        .update({
      'id': newid,
    });
  }

  inputDialog(id) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              title: Text(id,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Neue', fontWeight: FontWeight.w700)),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: TextFormField(
                      onSaved: (input) => newid = input,
                      validator: (input) {
                        return input == null || input == ''
                            ? 'Input cannot be empty'
                            : null;
                      },
                      decoration: InputDecoration(hintText: 'Change User ID'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.05),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: ButtonTheme(
                            height: MediaQuery.of(context).size.height * 0.08,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            buttonColor: Color.fromRGBO(255, 179, 77, 1),
                            child: RaisedButton(
                              child: Text(
                                'SAVE',
                                style: TextStyle(
                                  letterSpacing: 5.0,
                                  fontSize: 13.0,
                                  fontFamily: 'Neue',
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () {
                                if (validateForm()) {
                                  Navigator.pop(context);
                                  submitForm();
                                }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ));
  }

  showInputDialog() {
    inputDialog(fetchedUserid);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        onPressed: widget.edit ? showInputDialog : null,
        child: StreamBuilder(
            stream: FirebaseDatabase.instance
                .reference()
                .child('userdatas/')
                .onValue,
            builder: (context, snap) {
              if (snap.hasData &&
                  !snap.hasError &&
                  snap.data.snapshot.value != null) {
                if (snap.data.snapshot.value[widget.userid]['id'] == '') {
                  fetchedUserid = 'Set a User ID';
                  return Text(
                    'Set a User ID',
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Neue'),
                  );
                } else {
                  fetchedUserid = snap.data.snapshot.value[widget.userid]['id'];
                  return Text(
                    '@${snap.data.snapshot.value[widget.userid]['id']}',
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Neue'),
                  );
                }
              } else {
                fetchedUserid = '';
                return Text(
                  '',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w300,
                      fontFamily: 'Neue'),
                );
              }
            }),
      ),
    );
  }
}
