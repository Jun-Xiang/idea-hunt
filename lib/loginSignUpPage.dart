import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'auth.dart';
import 'package:firebase_auth/firebase_auth.dart';

enum FormType { login, signUp }

class LoginScreen extends StatefulWidget {
  LoginScreen({this.auth, this.onSignIn});
  final VoidCallback onSignIn;
  final BaseAuth auth;
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final String loginPath = 'assets/icons/logIn.svg';
  final String signUpPath = 'assets/icons/signUp.svg';
  final String emailPath = 'assets/icons/email.svg';
  final String passwordPath = 'assets/icons/password.svg';
  final String optionsWebDevPath = 'assets/icons/options_web_dev.svg';
  final String optionsUIUXPath = 'assets/icons/options_uiux.svg';
  final String optionsLogoDesignPath = 'assets/icons/options_logo_design.svg';
  final String optionsAppDevPath = 'assets/icons/options_app_dev.svg';
  final String namePath = 'assets/icons/name.svg';
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _email;
  String _password;
  FormType formType;
  bool validateForm() {
    final form = _formKey.currentState;
    form.save();
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  void _alertWrongEmailAndPassword(error) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error !'),
            content: Text(error),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  void signIn() {
    Navigator.pop(context);
    widget.onSignIn();
  }

  void _onSignUp(context, optionsSelected) {
    widget.auth.selection(_name, _email, _password, optionsSelected);
    widget.onSignIn();
  }

  void signUp() {
    bool userSubmitted = false;
    Navigator.pop(context);
    List selected = [
      {"option": "web-development", "selected": false, "color": Colors.white},
      {"option": "app-development", "selected": false, "color": Colors.white},
      {"option": "logo-design", "selected": false, "color": Colors.white},
      {"option": "ui/ux-design", "selected": false, "color": Colors.white}
    ];

    final String optionsWebDevPath = 'assets/icons/options_web_dev.svg';
    final String optionsUIUXPath = 'assets/icons/options_uiux.svg';
    final String optionsLogoDesignPath = 'assets/icons/options_logo_design.svg';
    final String optionsAppDevPath = 'assets/icons/options_app_dev.svg';
    var size = MediaQuery.of(context).size;
    var itemHeight = size.width * 0.4;
    var itemWidth = size.width * 0.3;

    void resetForm() async {
      if (userSubmitted == true) {
        return;
      }
      FirebaseUser user = await FirebaseAuth.instance.currentUser();
      user.delete();
      setState(() {
        selected.forEach((selection) {
          selection['selected'] = false;
          selection['color'] = Colors.white;
        });
      });
    }

    void submitForm(context) {
      List optionsSelected = [];
      selected.forEach((selection) {
        if (selection['selected'] == true) {
          optionsSelected.add(selection['option']);
        }
      });
      if (optionsSelected.length >= 1) {
        Navigator.pop(context);
        setState(() {
          userSubmitted = true;
        });
        _onSignUp(context, optionsSelected);
      } else {
        _alertWrongEmailAndPassword('Please select at least one!');
      }
    }

    var bottomsheet = showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            tapCard(index) {
              setState(() {
                selected[index]['selected'] = !(selected[index]['selected']);
                selected[index]['color'] =
                    selected[index]['selected'] ? Colors.grey : Colors.white;
              });
            }

            return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.9,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 179, 77, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(
                            top: 40,
                            bottom: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          'User Interest',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(fontFamily: 'Neue', fontSize: 25.0),
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01,
                          bottom: 30),
                      child: Text(
                        'Pick at least one from below',
                        style:
                            TextStyle(fontSize: 15.0, fontFamily: 'Neue Light'),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: GridView.count(
                        childAspectRatio: (itemWidth / itemHeight),
                        crossAxisCount: 2,
                        children: <Widget>[
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('Web Development'),
                            color: selected[0]['color'],
                            child: InkWell(
                              onTap: () => tapCard(0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsWebDevPath),
                                  ),
                                  Text('Web Development'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('App Development'),
                            color: selected[1]['color'],
                            child: InkWell(
                              focusColor: Colors.blueAccent,
                              onTap: () => tapCard(1),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsAppDevPath),
                                  ),
                                  Text('App Development'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('Logo Design'),
                            color: selected[2]['color'],
                            child: InkWell(
                              onTap: () => tapCard(2),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child:
                                        SvgPicture.asset(optionsLogoDesignPath),
                                  ),
                                  Text('Logo Design'),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            key: Key('UI/UX Design'),
                            color: selected[3]['color'],
                            child: InkWell(
                              onTap: () => tapCard(3),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: itemWidth * 0.1,
                                        bottom: itemWidth * 0.2),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    child: SvgPicture.asset(optionsUIUXPath),
                                  ),
                                  Text('UI/UX Design'),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 30),
                      child: ButtonTheme(
                        height: MediaQuery.of(context).size.height * 0.09,
                        minWidth: MediaQuery.of(context).size.width * 0.7,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        buttonColor: Colors.white,
                        child: RaisedButton(
                          child: Text(
                            'DONE!',
                            style: TextStyle(
                                letterSpacing: 5.0,
                                fontFamily: 'Neue',
                                fontSize: 15.0,
                                color: Color.fromRGBO(255, 179, 77, 1)),
                          ),
                          onPressed: () => submitForm(context),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
        });
    bottomsheet.whenComplete(resetForm);
  }

  void submitForm() async {
    if (validateForm()) {
      try {
        if (formType == FormType.login) {
          String userId = await widget.auth.signIn(_email, _password);
          userId.contains(' ') ? _alertWrongEmailAndPassword(userId) : signIn();
        } else if (formType == FormType.signUp) {
          String userId = await widget.auth.signUp(_email, _password);
          userId.contains(' ') ? _alertWrongEmailAndPassword(userId) : signUp();
        }
      } catch (e) {}
    }
  }

  homePage() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 179, 77, 1),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10.0, top: 30.0),
              child: Text(
                'Welcome!',
                textDirection: TextDirection.ltr,
                style: TextStyle(
                    fontFamily: 'Neue',
                    fontWeight: FontWeight.bold,
                    fontSize: 30.0),
              ),
            ),
            Text(
              'Login or Sign Up to continue',
              textDirection: TextDirection.ltr,
              style: TextStyle(fontSize: 15.0),
            ),
            Container(
              padding: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  mainScreenSelection(loginPath, 'Login'),
                  mainScreenSelection(signUpPath, 'Sign Up')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget mainScreenSelection(path, title) {
    return Padding(
        padding: EdgeInsets.fromLTRB(
            10,
            MediaQuery.of(context).size.width * 0.05,
            10,
            MediaQuery.of(context).size.width * 0.05),
        child: ButtonTheme(
          child: RaisedButton(
              onPressed: () {
                switch (title) {
                  case 'Login':
                    setState(() {
                      formType = FormType.login;
                    });
                    showModalBottomSheet(
                        context: context,
                        backgroundColor: Colors.transparent,
                        isScrollControlled: true,
                        builder: (BuildContext context) {
                          return SingleChildScrollView(
                            child: Container(
                              padding: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).viewInsets.bottom),
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 179, 77, 1),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.0),
                                    topRight: Radius.circular(30.0)),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.05,
                                          bottom: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01),
                                      child: Text(
                                        'Welcome back!',
                                        textDirection: TextDirection.ltr,
                                        style: TextStyle(
                                            fontFamily: 'Neue', fontSize: 25.0),
                                      )),
                                  Form(
                                      key: _formKey,
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 20.0,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1,
                                                right: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1),
                                            child: Stack(children: <Widget>[
                                              TextFormField(
                                                onSaved: (input) =>
                                                    _email = input,
                                                validator: (input) =>
                                                    input.contains('@')
                                                        ? null
                                                        : 'Not a valid email!',
                                                decoration: InputDecoration(
                                                  labelText: 'Email',
                                                ),
                                              ),
                                              Positioned(
                                                right: 0.0,
                                                bottom: 20,
                                                child:
                                                    SvgPicture.asset(emailPath),
                                              )
                                            ]),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1,
                                                right: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1,
                                                top: 20.0),
                                            child: Stack(children: <Widget>[
                                              TextFormField(
                                                onSaved: (input) =>
                                                    _password = input,
                                                validator: (input) => input
                                                            .length <
                                                        6
                                                    ? 'Password must be more than 6 characters'
                                                    : null,
                                                obscureText: true,
                                                decoration: InputDecoration(
                                                  labelText: 'Password',
                                                ),
                                              ),
                                              Positioned(
                                                right: 0.0,
                                                bottom: 20,
                                                child: SvgPicture.asset(
                                                    passwordPath),
                                              )
                                            ]),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 50.0, bottom: 40.0),
                                            child: ButtonTheme(
                                              child: RaisedButton(
                                                child: Text(
                                                  'LOGIN',
                                                  textDirection:
                                                      TextDirection.ltr,
                                                  style: TextStyle(
                                                      letterSpacing: 5.0,
                                                      fontSize: 15.0,
                                                      fontFamily: 'Neue',
                                                      color: Color.fromRGBO(
                                                          255, 179, 77, 1)),
                                                ),
                                                onPressed: submitForm,
                                              ),
                                              minWidth: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.7,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.09,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0)),
                                              buttonColor: Colors.white,
                                            ),
                                          )
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          );
                        });
                    break;
                  case 'Sign Up':
                    setState(() {
                      formType = FormType.signUp;
                    });
                    showModalBottomSheet(
                        context: context,
                        backgroundColor: Colors.transparent,
                        isScrollControlled: true,
                        builder: (BuildContext context) {
                          return SingleChildScrollView(
                            child: Container(
                                padding: EdgeInsets.only(
                                    bottom: MediaQuery.of(context)
                                        .viewInsets
                                        .bottom),
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 179, 77, 1),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(30.0),
                                      topRight: Radius.circular(30.0)),
                                ),
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.05,
                                              bottom: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            'Create Account',
                                            textDirection: TextDirection.ltr,
                                            style: TextStyle(
                                                fontFamily: 'Neue',
                                                fontSize: 25.0),
                                          )),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 20.0,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1),
                                        child: Stack(children: <Widget>[
                                          TextFormField(
                                            onSaved: (input) => _name = input,
                                            validator: (input) => input.length <
                                                    4
                                                ? 'Name must be longer than 3 characters'
                                                : null,
                                            decoration: InputDecoration(
                                              labelText: 'Name',
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            bottom: 20,
                                            child: SvgPicture.asset(namePath),
                                          )
                                        ]),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 20.0,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1),
                                        child: Stack(children: <Widget>[
                                          TextFormField(
                                            onSaved: (input) => _email = input,
                                            validator: (input) =>
                                                input.contains('@')
                                                    ? null
                                                    : 'Not a valid email!',
                                            decoration: InputDecoration(
                                              labelText: 'Email',
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            bottom: 20,
                                            child: SvgPicture.asset(emailPath),
                                          )
                                        ]),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            top: 20.0),
                                        child: Stack(children: <Widget>[
                                          TextFormField(
                                            onSaved: (input) =>
                                                _password = input,
                                            validator: (input) => input.length <
                                                    6
                                                ? 'Password must be more than 6 characters'
                                                : null,
                                            obscureText: true,
                                            decoration: InputDecoration(
                                              labelText: 'Password',
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            bottom: 20,
                                            child:
                                                SvgPicture.asset(passwordPath),
                                          )
                                        ]),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1,
                                            top: 20.0),
                                        child: Stack(children: <Widget>[
                                          TextFormField(
                                            validator: (input) =>
                                                input == _password
                                                    ? null
                                                    : 'Password Incorrect',
                                            obscureText: true,
                                            decoration: InputDecoration(
                                              labelText: 'Confirm Password',
                                            ),
                                          ),
                                          Positioned(
                                            right: 0.0,
                                            bottom: 20,
                                            child:
                                                SvgPicture.asset(passwordPath),
                                          )
                                        ]),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 50.0, bottom: 40.0),
                                        child: ButtonTheme(
                                          child: RaisedButton(
                                            child: Text(
                                              'CONTINUE',
                                              textDirection: TextDirection.ltr,
                                              style: TextStyle(
                                                  letterSpacing: 5.0,
                                                  fontSize: 15.0,
                                                  fontFamily: 'Neue',
                                                  color: Color.fromRGBO(
                                                      255, 179, 77, 1)),
                                            ),
                                            onPressed: submitForm,
                                          ),
                                          minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.7,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.09,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0)),
                                          buttonColor: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                          );
                        });
                    break;
                }
              },
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.width * 0.3,
                      child: SvgPicture.asset(path),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text(
                        title,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(fontSize: 15.0, fontFamily: 'Neue'),
                      ))
                ],
              )),
          minWidth: MediaQuery.of(context).size.width * 0.4,
          height: MediaQuery.of(context).size.height * 0.27,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          buttonColor: Colors.white,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return homePage();
  }
}
