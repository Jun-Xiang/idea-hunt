import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'auth.dart';
import 'dashboard.dart';
import 'loginSignUpPage.dart';
import 'settings.dart';
import 'addProjects.dart';
import 'about.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

enum AppState { loggedIn, none }

class _MyAppState extends State<MyApp> {
  void initState() {
    super.initState();
    Auth().currentUser().then((userId) {
      userId == null ? changeToNone() : changeToLogIn();
    });
  }

  AppState state = AppState.none;
  changeToLogIn() {
    setState(() {
      state = AppState.loggedIn;
    });
  }

  changeToNone() {
    setState(() {
      state = AppState.none;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget app;
    switch (state) {
      case AppState.loggedIn:
        app = Dashboard(auth: Auth());
        break;
      case AppState.none:
        app = HomePageView(changeToLogIn: changeToLogIn);
        break;
    }
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.black),
      home: app,
      routes: {
        '/settings': (BuildContext context) => Settings(
              auth: Auth(),
              logOut: changeToNone,
            ),
        '/addProjects': (BuildContext context) => AddProjectsPage(auth: Auth()),
        '/about': (BuildContext context) => About()
      },
    );
  }
}

class HomePageView extends StatefulWidget {
  HomePageView({this.changeToLogIn});
  final VoidCallback changeToLogIn;
  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (BuildContext context) => Column(children: <Widget>[
          Center(child: AppName()),
          LoginScreen(
            auth: Auth(),
            onSignIn: widget.changeToLogIn,
          ),
        ]),
      ),
      backgroundColor: Colors.white,
    );
  }
}

class AppName extends StatelessWidget {
  final String welcomePath = 'assets/icons/welcome.svg';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        height: MediaQuery.of(context).size.height * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.0),
              child: Text('IdeaHunt',
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      fontFamily: 'Neue',
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      decoration: TextDecoration.none)),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.height * 0.3,
              child: SvgPicture.asset(welcomePath),
            ),
          ],
        ),
      ),
    );
  }
}
