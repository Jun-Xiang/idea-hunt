import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);
  Future<String> signUp(String name, String email);
  Future<String> currentUser();
  Future<void> signOut();
  Future<void> selection(
      String name, String email, String password, optionsSelected);
}

class Auth implements BaseAuth {
  Future<String> signIn(String email, String password) async {
    try {
      AuthResult user = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      return user.user.uid;
    } catch (e) {
      return e.message;
    }
  }

  Future<String> signUp(String email, String password) async {
    try {
      AuthResult user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      return user.user.uid;
    } catch (e) {
      return e.message;
    }
  }

  Future<String> currentUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user.uid;
  }

  Future<void> signOut() async => FirebaseAuth.instance.signOut();
  Future<void> selection(
      String name, String email, String password, optionsSelected) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    FirebaseDatabase.instance.reference().child('userdatas/').update({
      user.uid: {
        'email': email,
        'id': '',
        'ideas': '',
        'interests': optionsSelected,
        'involved': '',
        'name': name,
        'password': password,
        'profilepicurl': '',
      }
    });
  }
}
