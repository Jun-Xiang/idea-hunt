import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dashboard.dart';
import 'auth.dart';
import 'settings.dart';

final databaseReference = FirebaseDatabase.instance.reference();

class ProjectsView extends StatefulWidget {
  final id;
  final projectid;
  final userid;
  ProjectsView({this.id, this.projectid, this.userid});
  @override
  _ProjectsViewState createState() => _ProjectsViewState();
}

class _ProjectsViewState extends State<ProjectsView> {
  var userid;
  @override
  void initState() {
    userid = Auth().currentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var comment = [];
    final String commentOrangePath = 'assets/icons/commentOrange.svg';
    final String likeOrangePath = 'assets/icons/likeOrange.svg';
    final String helpOrangePath = 'assets/icons/helpOrange.svg';
    final id = widget.id;
    final String backPath = 'assets/icons/back.svg';
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Container(
              padding: EdgeInsets.only(left: 10, bottom: 15.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: SizedBox(
                        height: 30,
                        width: 30,
                        child: SvgPicture.asset(backPath)),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Container(
          margin: EdgeInsets.only(
            left: MediaQuery.of(context).size.width * 0.05,
          ),
          width: MediaQuery.of(context).size.width * 0.9,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.02,
                      bottom: MediaQuery.of(context).size.height * 0.01),
                  child: StreamBuilder(
                      stream: databaseReference.child('projectdatas').onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          return Text(snap.data.snapshot.value[id]['name'],
                              style: TextStyle(
                                  fontSize: 30,
                                  fontFamily: 'Neue',
                                  fontWeight: FontWeight.bold));
                        } else {
                          return Text('Idea Title',
                              style: TextStyle(
                                  fontSize: 30,
                                  fontFamily: 'Neue',
                                  fontWeight: FontWeight.bold));
                        }
                      }),
                ),
                Container(
                  height: 30,
                  width: 250,
                  child: StreamBuilder(
                      stream: databaseReference.child('projectdatas').onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          if (snap.data.snapshot.value[id]['tags'] != null) {
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    snap.data.snapshot.value[id]['tags'].length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    padding: EdgeInsets.only(right: 2),
                                    child: Chip(
                                      labelPadding: EdgeInsets.fromLTRB(
                                          -0.5, -5, -0.5, -5),
                                      label: Text(
                                          snap.data.snapshot.value[id]['tags']
                                              [index],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10)),
                                      backgroundColor:
                                          Color.fromRGBO(0, 255, 156, 1),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(6.0))),
                                    ),
                                  );
                                });
                          } else {
                            return Padding(
                              padding: EdgeInsets.only(right: 2),
                              child: Text(
                                'No tags',
                                style: TextStyle(
                                    fontFamily: 'Neue',
                                    fontSize: 15,
                                    color: Colors.grey[400]),
                              ),
                            );
                          }
                        } else {
                          return Padding(padding: EdgeInsets.only(right: 2));
                        }
                      }),
                ),
                StreamBuilder(
                    stream: databaseReference.child('projectdatas').onValue,
                    builder: (context, snap) {
                      if (snap.hasData &&
                          !snap.hasError &&
                          snap.data.snapshot.value != null) {
                        return Row(
                          children: <Widget>[
                            ProfilePic(
                              auth: Auth(),
                              setWH: 0.06,
                              setMargin: 0,
                              handlePress: () {},
                            ),
                            UserID(
                              auth: Auth(),
                              edit: false,
                              userid: snap.data.snapshot.value[id]['host'],
                            ),
                          ],
                        );
                      } else {
                        return Container(
                          padding: EdgeInsets.only(top: 5),
                        );
                      }
                    }),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          bottom: MediaQuery.of(context).size.width * 0.03,
                          right: MediaQuery.of(context).size.width * 0.03),
                      child: Likes(
                        auth: Auth(),
                        id: id,
                        view: 'Row',
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          bottom: MediaQuery.of(context).size.width * 0.03,
                          right: MediaQuery.of(context).size.width * 0.03),
                      child: Comments(
                        auth: Auth(),
                        id: id,
                        view: 'Row',
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          bottom: MediaQuery.of(context).size.width * 0.03,
                          right: MediaQuery.of(context).size.width * 0.03),
                      child: Accepts(
                        auth: Auth(),
                        id: id,
                        view: 'Row',
                      ),
                    ),
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.9,
                  margin: EdgeInsets.only(
                      bottom: MediaQuery.of(context).size.height * 0.03,
                      top: MediaQuery.of(context).size.height * 0.01),
                  child: StreamBuilder(
                      stream: databaseReference.child('projectdatas').onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          return SingleChildScrollView(
                            child: Text(
                              snap.data.snapshot.value[id]['desc'],
                              style: TextStyle(
                                  fontFamily: 'Neue',
                                  fontWeight: FontWeight.w300,
                                  fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                          );
                        } else {
                          return Container(
                            padding: EdgeInsets.only(top: 5),
                          );
                        }
                      }),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.2 + 10,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.03),
                  child: StreamBuilder(
                      stream: databaseReference.child('projectdatas').onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              itemCount: snap.data.snapshot
                                  .value[widget.id]['comments'].length,
                              itemBuilder: (context, i) {
                                return CommentBlocks(id: snap.data.snapshot
                                    .value[widget.id]['comments'][i]['userid'],title: snap.data.snapshot
                                    .value[widget.id]['comments'][i]['commenttitle'],comment: snap.data.snapshot
                                    .value[widget.id]['comments'][i]['commentfull'],likes: snap.data.snapshot
                                    .value[widget.id]['comments'][i]['upvotes']);
                              });
                        } else {
                          return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              itemCount: 0,
                              itemBuilder: (context, i) {
                                return CommentBlocks();
                              });
                        }
                      }),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ActionButton(
                        path: likeOrangePath,
                        type: 'like',
                        id: widget.id,
                        userid: widget.userid,
                        projectid: widget.projectid,
                      ),
                      ActionButton(
                        path: commentOrangePath,
                        type: 'comment',
                        id: widget.id,
                        userid: widget.userid,
                        projectid: widget.projectid,
                      ),
                      ActionButton(
                        path: helpOrangePath,
                        type: 'help',
                        id: widget.id,
                        userid: widget.userid,
                        projectid: widget.projectid,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ActionButton extends StatefulWidget {
  final String path;
  final type;
  final id;
  final userid;
  final projectid;
  ActionButton({this.path, this.type, this.id, this.userid, this.projectid});
  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  var joinStatus = false;

  inputComment() {
    final controller = TextEditingController();
    final titleController = TextEditingController();
    var commentText;
    var commentTitle;
    var userid = widget.userid;
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.1,left: MediaQuery.of(context).size.width * 0.1, right: MediaQuery.of(context).size.width * 0.1),
          height: MediaQuery.of(context).size.height * 0.5 + MediaQuery.of(context).viewInsets.bottom,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05, top: MediaQuery.of(context).size.height * 0.01),
                child: TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                    hintText:  'Title',
                  ),
                ),
              ),

              TextField(
                controller:  controller,
                decoration: InputDecoration(
                  hintText: 'Write a comment...',
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width* 0.1, MediaQuery.of(context).size.width* 0.15, MediaQuery.of(context).size.width* 0.1,MediaQuery.of(context).size.width* 0.1),
                child: ButtonTheme(
                  height: MediaQuery.of(context).size.height * 0.1,
                  minWidth: MediaQuery.of(context).size.width * 0.65,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Color.fromRGBO(255, 179, 77, 1),
                    child: Text('COMMENT', style: TextStyle(fontFamily: 'Neue', color: Colors.white, letterSpacing: 5.0),),
                    onPressed: (){
                      if(controller.text.length == 0 && titleController.text.length == 0){
                        Navigator.pop(context);
                        showDialog(
                          context: context,
                          builder: (context) =>
                            AlertDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              title: Text('Comments cannot be empty!', style: TextStyle(fontFamily: 'Neue'),),
                              content: Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05, bottom: MediaQuery.of(context).size.height * 0.01),
                                child: ButtonTheme(
                                  height: MediaQuery.of(context).size.height * 0.08,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: RaisedButton(
                                    color: Color.fromRGBO(255, 179, 77, 1),
                                    child: Text('OK', style: TextStyle(fontFamily: 'Neue', letterSpacing: 4.0, color: Colors.white),),
                                    onPressed: (){
                                      Navigator.pop(context);
                                    },
                                  ),
                                ),
                              )
                          )
                        );
                      }else{
                        commentTitle = titleController.text;
                        commentText = controller.text;
                        databaseReference
                            .child('projectdatas/' + widget.id.toString() + '/comments')
                            .once()
                            .then((DataSnapshot snapshot) {
                          if (snapshot.value == '') {
                            List synclist = [];
                            Object newComment = {
                              'userid' : widget.userid,
                              'commenttitle' : commentTitle,
                              'commentfull' : commentText,
                              'upvotes' : ''
                            };
                            synclist.add(newComment);
                            var updateLikesRef = FirebaseDatabase.instance
                                .reference()
                                .child(
                                'projectdatas/' + widget.id.toString() + '/comments');
                            updateLikesRef.set(synclist);
                          } else {
                            var synclist = new List<dynamic>.from(snapshot.value);
                            Object newComment = {
                              'userid' : widget.userid,
                              'commenttitle' : commentTitle,
                              'commentfull' : commentText,
                              'upvotes' : ''
                            };
                            synclist.add(newComment);
                            var updateLikesRef = FirebaseDatabase.instance
                                .reference()
                                .child(
                                'projectdatas/' + widget.id.toString() + '/comments');
                            updateLikesRef.set(synclist);
                          }
                        });
                        Navigator.pop(context);
                      }
                    },
                  ),
                ),
              )

            ],
          ),
        ));
  }

  var pressed = false;
  handlePress() {
    switch (widget.type) {
      case 'like':
        setState(() {
          pressed = !pressed;
          Auth().currentUser().then((String returnedID) {
            if (pressed) {
              databaseReference
                  .child('projectdatas/' + widget.id.toString() + '/upvotes')
                  .once()
                  .then((DataSnapshot snapshot) {
                if (snapshot.value == '') {
                  List synclist = [];
                  synclist.add(returnedID);
                  var updateLikesRef = FirebaseDatabase.instance
                      .reference()
                      .child(
                          'projectdatas/' + widget.id.toString() + '/upvotes');
                  updateLikesRef.set(synclist);
                } else {
                  var synclist = new List<dynamic>.from(snapshot.value);
                  synclist.add(returnedID);
                  var updateLikesRef = FirebaseDatabase.instance
                      .reference()
                      .child(
                          'projectdatas/' + widget.id.toString() + '/upvotes');
                  updateLikesRef.set(synclist);
                }
              });
            } else {
              databaseReference
                  .child('projectdatas/' + widget.id.toString() + '/upvotes')
                  .once()
                  .then((DataSnapshot snapshot) {
                var synclist = new List<dynamic>.from(snapshot.value);
                synclist.remove(returnedID);
                if (synclist.length == 0) {
                  var updateLikesRef = FirebaseDatabase.instance
                      .reference()
                      .child(
                          'projectdatas/' + widget.id.toString() + '/upvotes');
                  updateLikesRef.set('');
                } else {
                  var updateLikesRef = FirebaseDatabase.instance
                      .reference()
                      .child(
                          'projectdatas/' + widget.id.toString() + '/upvotes');
                  updateLikesRef.set(synclist);
                }
              });
            }
          });
        });
        break;
      case 'comment':
        inputComment();
        break;
      case 'help':
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  title: Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.03),
                    child: StreamBuilder(
                        stream: databaseReference
                            .child('projectdatas/' + widget.id.toString())
                            .onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            if (snap.data.snapshot.value['involved'] == '') {
                              return Text(
                                'Confirm Join?',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.w500),
                              );
                            } else {
                              joinStatus = false;
                              for (var i = 0;
                                  i <
                                      snap.data.snapshot.value['involved']
                                          .length;
                                  i++) {
                                if (snap.data.snapshot.value['involved'][i] ==
                                    widget.userid) {
                                  joinStatus = true;
                                }
                              }
                              if (joinStatus == false) {
                                return Text(
                                  'Confirm Join?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontFamily: 'Neue',
                                      fontWeight: FontWeight.w500),
                                );
                              } else {
                                return Text(
                                  'Confirm Leave?',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontFamily: 'Neue',
                                      fontWeight: FontWeight.w500),
                                );
                              }
                            }
                          } else {
                            return Text(
                              'Gettings Data',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontFamily: 'Neue',
                                  fontWeight: FontWeight.w500),
                            );
                          }
                        }),
                  ),
                  content: ButtonTheme(
                    height: MediaQuery.of(context).size.height * 0.08,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    buttonColor: Color.fromRGBO(255, 179, 77, 1),
                    child: StreamBuilder(
                        stream: databaseReference
                            .child('projectdatas/' + widget.id.toString())
                            .onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            if (snap.data.snapshot.value['involved'] == '') {
                              return RaisedButton(
                                child: Text(
                                  'JOIN',
                                  style: TextStyle(
                                      color: Colors.white,
                                      letterSpacing: 3.0,
                                      fontFamily: 'Neue',
                                      fontSize: 15.0),
                                ),
                                onPressed: () {
                                  Auth()
                                      .currentUser()
                                      .then((String returnedID) {
                                    databaseReference
                                        .child('projectdatas/' +
                                            widget.id.toString() +
                                            '/involved')
                                        .once()
                                        .then((DataSnapshot snapshot) {
                                      if (snapshot.value == '') {
                                        List synclist = [];
                                        synclist.add(returnedID);
                                        var updateInvolvedRef = FirebaseDatabase
                                            .instance
                                            .reference()
                                            .child('projectdatas/' +
                                                widget.id.toString() +
                                                '/involved');
                                        updateInvolvedRef.set(synclist);
                                        databaseReference
                                            .child('userdatas/' +
                                                widget.userid.toString())
                                            .once()
                                            .then((DataSnapshot snapshot) {
                                          if (snapshot.value['involved'] ==
                                              '') {
                                            List synclist2 = [];
                                            synclist2.add(widget.projectid);
                                            var updateInvolvedRef =
                                                FirebaseDatabase
                                                    .instance
                                                    .reference()
                                                    .child('userdatas/' +
                                                        widget.userid
                                                            .toString() +
                                                        '/involved');
                                            updateInvolvedRef.set(synclist2);
                                          } else {
                                            var synclist2 =
                                                new List<String>.from(
                                                    snapshot.value['involved']);
                                            synclist2.add(widget.projectid);
                                            var updateInvolvedRef =
                                                FirebaseDatabase
                                                    .instance
                                                    .reference()
                                                    .child('userdatas/' +
                                                        widget.userid
                                                            .toString() +
                                                        '/involved');
                                            updateInvolvedRef.set(synclist2);
                                          }
                                        });
                                      } else {
                                        var statusCheck = true;
                                        var synclist = new List<dynamic>.from(
                                            snapshot.value);
                                        for (var i = 0;
                                            i < snapshot.value.length;
                                            i++) {
                                          if (snapshot.value[i] == returnedID) {
                                            statusCheck = false;
                                          }
                                        }
                                        if (statusCheck == true) {
                                          var synclist = new List<dynamic>.from(
                                              snapshot.value);
                                          synclist.add(returnedID);
                                          var updateInvolvedRef =
                                              FirebaseDatabase.instance
                                                  .reference()
                                                  .child('projectdatas/' +
                                                      widget.id.toString() +
                                                      '/involved');
                                          updateInvolvedRef.set(synclist);
                                          databaseReference
                                              .child('userdatas/' +
                                                  widget.userid.toString())
                                              .once()
                                              .then((DataSnapshot snapshot) {
                                            if (snapshot.value['involved'] ==
                                                '') {
                                              List synclist2 = [];
                                              synclist2.add(widget.projectid);
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            } else {
                                              var synclist2 =
                                                  new List<String>.from(snapshot
                                                      .value['involved']);
                                              synclist2.add(widget.projectid);
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            }
                                          });
                                        }
                                      }
                                    });
                                  });
                                  Navigator.pop(context);
                                },
                              );
                            } else {
                              joinStatus = false;
                              for (var i = 0;
                                  i <
                                      snap.data.snapshot.value['involved']
                                          .length;
                                  i++) {
                                if (snap.data.snapshot.value['involved'][i] ==
                                    widget.userid) {
                                  joinStatus = true;
                                }
                              }
                              if (joinStatus == false) {
                                return RaisedButton(
                                  child: Text(
                                    'JOIN',
                                    style: TextStyle(
                                        color: Colors.white,
                                        letterSpacing: 3.0,
                                        fontFamily: 'Neue',
                                        fontSize: 15.0),
                                  ),
                                  onPressed: () {
                                    Auth()
                                        .currentUser()
                                        .then((String returnedID) {
                                      databaseReference
                                          .child('projectdatas/' +
                                              widget.id.toString() +
                                              '/involved')
                                          .once()
                                          .then((DataSnapshot snapshot) {
                                        if (snapshot.value == '') {
                                          List synclist = [];
                                          synclist.add(returnedID);
                                          var updateInvolvedRef =
                                              FirebaseDatabase.instance
                                                  .reference()
                                                  .child('projectdatas/' +
                                                      widget.id.toString() +
                                                      '/involved');
                                          updateInvolvedRef.set(synclist);
                                          databaseReference
                                              .child('userdatas/' +
                                                  widget.userid.toString())
                                              .once()
                                              .then((DataSnapshot snapshot) {
                                            if (snapshot.value['involved'] ==
                                                '') {
                                              List synclist2 = [];
                                              synclist2.add(widget.projectid);
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            } else {
                                              var synclist2 =
                                                  new List<String>.from(snapshot
                                                      .value['involved']);
                                              synclist2.add(widget.projectid);
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            }
                                          });
                                        } else {
                                          var statusCheck = true;
                                          var synclist = new List<dynamic>.from(
                                              snapshot.value);
                                          for (var i = 0;
                                              i < snapshot.value.length;
                                              i++) {
                                            if (snapshot.value[i] ==
                                                returnedID) {
                                              statusCheck = false;
                                            }
                                          }
                                          if (statusCheck == true) {
                                            var synclist =
                                                new List<dynamic>.from(
                                                    snapshot.value);
                                            synclist.add(returnedID);
                                            var updateInvolvedRef =
                                                FirebaseDatabase
                                                    .instance
                                                    .reference()
                                                    .child('projectdatas/' +
                                                        widget.id.toString() +
                                                        '/involved');
                                            updateInvolvedRef.set(synclist);
                                            databaseReference
                                                .child('userdatas/' +
                                                    widget.userid.toString())
                                                .once()
                                                .then((DataSnapshot snapshot) {
                                              if (snapshot.value['involved'] ==
                                                  '') {
                                                List synclist2 = [];
                                                synclist2.add(widget.projectid);
                                                var updateInvolvedRef =
                                                    FirebaseDatabase.instance
                                                        .reference()
                                                        .child('userdatas/' +
                                                            widget.userid
                                                                .toString() +
                                                            '/involved');
                                                updateInvolvedRef
                                                    .set(synclist2);
                                              } else {
                                                var synclist2 =
                                                    new List<String>.from(
                                                        snapshot
                                                            .value['involved']);
                                                synclist2.add(widget.projectid);
                                                var updateInvolvedRef =
                                                    FirebaseDatabase.instance
                                                        .reference()
                                                        .child('userdatas/' +
                                                            widget.userid
                                                                .toString() +
                                                            '/involved');
                                                updateInvolvedRef
                                                    .set(synclist2);
                                              }
                                            });
                                          }
                                        }
                                      });
                                    });
                                    Navigator.pop(context);
                                  },
                                );
                              } else {
                                return RaisedButton(
                                  child: Text(
                                    'LEAVE',
                                    style: TextStyle(
                                        color: Colors.white,
                                        letterSpacing: 3.0,
                                        fontFamily: 'Neue',
                                        fontSize: 15.0),
                                  ),
                                  onPressed: () {
                                    Auth()
                                        .currentUser()
                                        .then((String returnedID) {
                                      databaseReference
                                          .child('projectdatas/' +
                                              widget.id.toString() +
                                              '/involved')
                                          .once()
                                          .then((DataSnapshot snapshot) {
                                        var synclist = new List<dynamic>.from(
                                            snapshot.value);
                                        synclist.remove(returnedID);
                                        if (synclist.length == 0) {
                                          var updateLikesRef = FirebaseDatabase
                                              .instance
                                              .reference()
                                              .child('projectdatas/' +
                                                  widget.id.toString() +
                                                  '/involved');
                                          updateLikesRef.set('');
                                          databaseReference
                                              .child('userdatas/' +
                                                  widget.userid.toString())
                                              .once()
                                              .then((DataSnapshot snapshot) {
                                            var synclist2 =
                                                new List<String>.from(
                                                    snapshot.value['involved']);
                                            synclist2.remove(widget.projectid);
                                            if (synclist2.length == 0) {
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set('');
                                            } else {
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            }
                                          });
                                        } else {
                                          var updateLikesRef = FirebaseDatabase
                                              .instance
                                              .reference()
                                              .child('projectdatas/' +
                                                  widget.id.toString() +
                                                  '/involved');
                                          updateLikesRef.set(synclist);
                                          databaseReference
                                              .child('userdatas/' +
                                                  widget.userid.toString())
                                              .once()
                                              .then((DataSnapshot snapshot) {
                                            var synclist2 =
                                                new List<String>.from(
                                                    snapshot.value['involved']);
                                            synclist2.remove(widget.projectid);
                                            if (synclist2.length == 0) {
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set('');
                                            } else {
                                              var updateInvolvedRef =
                                                  FirebaseDatabase.instance
                                                      .reference()
                                                      .child('userdatas/' +
                                                          widget.userid
                                                              .toString() +
                                                          '/involved');
                                              updateInvolvedRef.set(synclist2);
                                            }
                                          });
                                        }
                                      });
                                    });
                                    Navigator.pop(context);
                                  },
                                );
                              }
                            }
                          } else {
                            return RaisedButton(
                              child: Text(
                                'JOIN',
                                style: TextStyle(
                                    color: Colors.white,
                                    letterSpacing: 3.0,
                                    fontFamily: 'Neue',
                                    fontSize: 15.0),
                              ),
                              onPressed: null,
                            );
                          }
                        }),
                  ),
                ));
        break;
    }
  }

  @override
  void initState() {
    Auth().currentUser().then((String returnedID) {
      databaseReference
          .child('projectdatas/')
          .once()
          .then((DataSnapshot snapshot) {
        if (snapshot.value[widget.id]['upvotes'] != '') {
          for (var i = 0;
              i <= snapshot.value[widget.id]['upvotes'].length - 1;
              i++) {
            if (snapshot.value[widget.id]['upvotes'][i] == returnedID) {
              setState(() {
                widget.type == 'like' ? pressed = true : null;
              });
            }
          }
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var pressedIcon;
    var pressLike = 'assets/icons/likeWhite.svg';
    var pressComment = 'assets/icons/commentWhite.svg';
    var pressHelp = 'assets/icons/helpWhite.svg';
    switch (widget.type) {
      case 'like':
        pressedIcon = pressLike;
        break;
      case 'comment':
//        pressedIcon = pressComment;
        break;
      case 'help':
        pressedIcon = joinStatus ? pressHelp : widget.path;
        break;
    }
    var path = pressed ? pressedIcon : widget.path;
    var color = pressed ? Color.fromRGBO(255, 179, 77, 1) : Colors.transparent;
    return Container(
      width: MediaQuery.of(context).size.height * 0.08,
      height: MediaQuery.of(context).size.height * 0.08,
      decoration: BoxDecoration(
        color: widget.type == 'help' ? Colors.transparent : color,
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: FlatButton(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: SvgPicture.asset(path),
        onPressed: handlePress,
      ),
    );
  }
}

class CommentBlocks extends StatefulWidget {
  final id;
  final title;
  final comment;
  final likes;
  @override
  CommentBlocks({this.id,this.title,this.comment,this.likes});
  _CommentBlocksState createState() => _CommentBlocksState();
}

class _CommentBlocksState extends State<CommentBlocks> {
  @override
  Widget build(BuildContext context) {
    final testID = widget.id ?? 'smYQQSA07HO0W1eEgDisDSY9DM42';
    return Container(
        margin: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.grey.withOpacity(0.3),
        ),
        height: MediaQuery.of(context).size.height * 0.3,
        width: MediaQuery.of(context).size.width * 0.85,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ProfilePic(
                    commentId: testID,
                    setWH: 0.03,
                    setMargin: 0.02,
                    handlePress: () {},
                  ),
                  Container(
                      height: 20,
                      width: MediaQuery.of(context).size.width * 0.65,
                      child: Text(
                        widget.title,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Neue'),
                      ))
                ],
              ),
            ),
            Flexible(
                child: Container(
              height: MediaQuery.of(context).size.height * 0.08,
              width: MediaQuery.of(context).size.width * 0.73,
              margin: EdgeInsets.all(10.0),
              child: Text(
                widget.comment,
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.justify,
                style: TextStyle(
                    fontFamily: 'Neue',
                    fontSize: 10.0,
                    fontWeight: FontWeight.w300),
              ),
            )),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.02,
                    right: MediaQuery.of(context).size.width * 0.06),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 5),
                      height: 15,
                      width: 15,
                      child: SvgPicture.asset('assets/icons/likeBlack.svg'),
                    ),
                    Container(
                      child: Text(widget.likes.length.toString(),
                          style: TextStyle(fontFamily: 'Neue', fontSize: 10)),
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
