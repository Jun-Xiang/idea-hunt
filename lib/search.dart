import 'package:flutter/material.dart';
import 'dashboard.dart';
import 'auth.dart';
import 'projectsView.dart';
import 'package:firebase_database/firebase_database.dart';

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  List<Widget> searchRes = [];
  preview(id){
    setState(() {
      searchRes = [];
    });
    for(var i = 0; i < id.length; i++){
      setState(() {
        searchRes.add(SearchBlock(projectId: id[i]));
      });
    }
  }
  filter(String input,List data){
    List filteredData = [];
    for (var i = 0;i <= data.length - 1;i++) {
      String test = data[i]['name'];
      test= test.toUpperCase();
      input = input.toUpperCase();
      if(test.indexOf(input) != -1){
        filteredData.add(data[i]['id']);
      }
    }
    preview(filteredData);
  }
  final _controller = TextEditingController();


  @override
  Widget build(BuildContext context) {
    var fetchedDatas;
    FirebaseDatabase.instance.reference().child('projectdatas').once().then((DataSnapshot snapshot) {
      fetchedDatas = snapshot.value;
    });
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 15),
          height: 100,
          child: Stack(children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.1 , 20.0,MediaQuery.of(context).size.width * 0.1, 20.0),
              child: TextField(
                controller: _controller,
                onSubmitted: (input){
                  filter(_controller.text,fetchedDatas);
                },
                style: TextStyle(fontSize: 20.0),
                decoration: InputDecoration(
                    fillColor: Colors.grey[200],
                    filled: true,
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.transparent)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.transparent)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.transparent))),
              ),
            ),
            Positioned(
              top: 23.0,
              right: MediaQuery.of(context).size.width * 0.1,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.2 - 20.0,
                height: MediaQuery.of(context).size.width * 0.2 - 20.0,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0)
                  ),
                  onPressed: (){
                    FocusScope.of(context).requestFocus(FocusNode());
                    filter(_controller.text,fetchedDatas);
                  },
                  child: Icon(Icons.search)
                ),
              ),
            ),
          ]),
        ),
        Column(mainAxisSize: MainAxisSize.min, children: searchRes )
      ],
    );
  }
}

class SearchBlock extends StatefulWidget {
  final userid;
  final projectId;
  SearchBlock({ this.userid, this.projectId });
  @override
  _SearchBlockState createState() => _SearchBlockState();
}

class _SearchBlockState extends State<SearchBlock> {
  List projectId = [];
  Widget view(id) {
//    projectId = id;
    return Container(
        width: MediaQuery.of(context).size.width * 0.95,
        margin: EdgeInsets.only(
            top: 10,
            left: MediaQuery.of(context).size.width * 0.05,
            right: MediaQuery.of(context).size.width * 0.05),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.grey[200],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 4.0, left: 15.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 250,
                    child: StreamBuilder(
                        stream:
                        databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var chipIndex;
                            for (var i = 0;
                            i < snap.data.snapshot.value.length;
                            i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                chipIndex = i;
                              }
                            }
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snap.data.snapshot
                                    .value[chipIndex]['tags'].length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: EdgeInsets.only(right: 6),
                                    child: Chip(
                                      label: Text(
                                          snap.data.snapshot.value[chipIndex]
                                          ['tags'][index],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12)),
                                      backgroundColor:
                                      Color.fromRGBO(0, 255, 156, 1),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(6.0))),
                                    ),
                                  );
                                });
                          } else {
                            return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 0,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: EdgeInsets.only(right: 6),
                                    child: Chip(
                                      label: Text('chiplabel1',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12)),
                                      backgroundColor:
                                      Color.fromRGBO(0, 255, 156, 1),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(6.0))),
                                    ),
                                  );
                                });
                          }
                        }),
                  ),
                  StreamBuilder(
                      stream: databaseReference.onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          var picIndex;
                          var picurl;
                          for (var i = 0;
                          i <
                              snap.data.snapshot.value['projectdatas']
                                  .length;
                          i++) {
                            if (snap.data.snapshot.value['projectdatas'][i]
                            ['id'] ==
                                id) {
                              picIndex = i;
                            }
                          }
                          if (snap.data.snapshot.value['userdatas'][
                          snap.data.snapshot.value['projectdatas']
                          [picIndex]['host']]['profilepicurl'] !=
                              '') {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(snap
                                          .data.snapshot.value['userdatas'][snap
                                          .data
                                          .snapshot
                                          .value['projectdatas'][picIndex]
                                      ['host']]['profilepicurl'])),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6.0))),
                            );
                          } else {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/profpic.png')),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6.0))),
                            );
                          }
                        } else {
                          return Container(
                            margin: EdgeInsets.only(right: 15),
                            height: 27,
                            width: 27,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/profpic.png')),
                                borderRadius:
                                BorderRadius.all(Radius.circular(6.0))),
                          );
                        }
                      })
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                        databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var ideaIndex;
                            for (var i = 0;
                            i < snap.data.snapshot.value.length;
                            i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                ideaIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[ideaIndex]['name'],
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          } else {
                            return Text('',
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                        databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var descIndex;
                            for (var i = 0;
                            i < snap.data.snapshot.value.length;
                            i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                descIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[descIndex]['desc'],
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          } else {
                            return Text('',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 18, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Likes(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                        Comments(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                        Accepts(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 22),
                      child: StreamBuilder(
                          stream:
                          databaseReference.child('projectdatas/').onValue,
                          builder: (context, snap) {
                            if (snap.hasData &&
                                !snap.hasError &&
                                snap.data.snapshot.value != null) {
                              var idIndex;
                              for (var i = 0;
                              i < snap.data.snapshot.value.length;
                              i++) {
                                if (snap.data.snapshot.value[i]['id'] == id) {
                                  idIndex = i;
                                }
                              }
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                        return ProjectsView(
                                          id: idIndex,
                                          userid: widget.userid,
                                          projectid: id,
                                        );
                                      }));
                                },
                              );
                            } else {
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: null,
                              );
                            }
                          }),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }


  @override
  Widget build(BuildContext context) {
    return view(widget.projectId);
  }
}
