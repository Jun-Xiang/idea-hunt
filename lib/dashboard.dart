import 'package:flutter/cupertino.dart';
import 'auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'search.dart';
import 'projects.dart';
import 'package:firebase_database/firebase_database.dart';
import 'projectsView.dart';

final projectDataReferenceRecent =
    FirebaseDatabase.instance.reference().child('projectdatas');

class Dashboard extends StatefulWidget {
  Dashboard({this.auth});
  final BaseAuth auth;
  @override
  State<StatefulWidget> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final String homePath = 'assets/icons/homeWhite.svg';
  final String searchPath = 'assets/icons/searchWhite.svg';
  final String projectPath = 'assets/icons/projectWhite.svg';
  var title;
  var showPage;
  var content;
  var selectedNum = 2;

  var userid;
  @override
  void initState() {
    super.initState();
    widget.auth.currentUser().then((String getuserid) {
      setState(() {
        userid = getuserid;
      });
    });
  }

  homepageView() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.05,
                left: MediaQuery.of(context).size.width * 0.05),
            child: Text(
              'Trending Ideas',
              style: TextStyle(
                fontFamily: 'Neue Light',
                fontSize: 30.0,
              ),
            ),
          ),
          Container(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
              child: Container(
                child: Center(
                    child: Text(
                  'Feature Coming Soon',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Neue',
                      fontSize: 20),
                )),
              ))
        ],
      );

  void select(num) {
    setState(() {
      selectedNum = num;
    });
  }

  Widget bottomNavBar() => Positioned(
        bottom: 0,
        right: 0,
        left: 0,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40.0),
                topRight: Radius.circular(40.0)),
            color: Color.fromRGBO(255, 179, 77, 1),
          ),
          height: MediaQuery.of(context).size.height * 0.11,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BottomIcon(searchPath, select, 1, 1 == selectedNum),
              BottomIcon(homePath, select, 2, 2 == selectedNum),
              BottomIcon(projectPath, select, 3, 3 == selectedNum),
            ],
          ),
        ),
      );

  openSettings() {
    Navigator.pushNamed(context, '/settings');
  }

  addProjects() {
    openAddProject() {
      Navigator.pushNamed(context, '/addProjects');
    }

    return Container(
      margin: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.04,
          bottom: MediaQuery.of(context).size.height * 0.04),
      child: Center(
        child: ButtonTheme(
          buttonColor: Color.fromRGBO(255, 179, 77, 1),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          height: MediaQuery.of(context).size.height * 0.1,
          minWidth: MediaQuery.of(context).size.width * 0.7,
          child: RaisedButton(
            onPressed: openAddProject,
            child: Text(
              'ADD NEW IDEA',
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 6.0,
                  fontSize: 12,
                  fontFamily: 'Neue',
                  fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (selectedNum) {
      case 1:
        setState(() {
          content = Container();
          showPage = SearchBar();
          title = 'Search Ideas';
        });
        break;
      case 2:
        setState(() {
          content = RecentView(userid: userid);
          showPage = homepageView();
          title = 'IdeaHunt';
        });
        break;
      case 3:
        setState(() {
          content = Projects(usrid: userid);
          showPage = addProjects();
          title = 'My Ideas';
        });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Builder(
        builder: (BuildContext context) => SafeArea(
            child: Stack(children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ListView(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.15),
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.05),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.08),
                              child: Text(
                                title,
                                style: TextStyle(
                                    fontFamily: 'Neue',
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            ProfilePic(
                              auth: Auth(),
                              handlePress: openSettings,
                              setWH: 0.08,
                              setMargin: 0.08,
                            )
                          ],
                        ),
                      ),
                      showPage
                    ],
                  ),
                  content
                ]),
          ),
          bottomNavBar()
        ])),
      ),
    );
  }
}

class ProfilePic extends StatefulWidget {
  final BaseAuth auth;
  final handlePress;
  final setWH;
  final setMargin;
  final commentId;
  final acceptId;
  final color;
  ProfilePic(
      {this.auth,
      this.handlePress,
      this.setWH,
      this.setMargin,
      this.commentId,
      this.acceptId,
      this.color});
  @override
  _ProfilePicState createState() => _ProfilePicState();
}

class _ProfilePicState extends State<ProfilePic> {
  var userid1;
  @override
  void initState() {
    super.initState();
    widget.auth != null
        ? widget.auth.currentUser().then((String getuserid) {
            setState(() {
              userid1 = getuserid;
            });
          })
        : userid1 = widget.commentId ?? widget.acceptId;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream:
            FirebaseDatabase.instance.reference().child('userdatas/').onValue,
        builder: (context, snap) {
          if (snap.hasData &&
              !snap.hasError &&
              snap.data.snapshot.value != null) {
            if (snap.data.snapshot.value[userid1]['profilepicurl'] == '') {
              return Container(
                child: Container(
                  width: MediaQuery.of(context).size.height * widget.setWH,
                  height: MediaQuery.of(context).size.height * widget.setWH,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    onPressed: widget.handlePress,
                  ),
                ),
                width: MediaQuery.of(context).size.height * widget.setWH,
                height: MediaQuery.of(context).size.height * widget.setWH,
                decoration: BoxDecoration(
                  color: widget.color ?? Colors.grey,
                  image: DecorationImage(
                      image: AssetImage('assets/images/profpic.png')),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                margin: EdgeInsets.only(
                    right:
                        MediaQuery.of(context).size.width * widget.setMargin),
              );
            } else {
              return Container(
                child: Container(
                  width: MediaQuery.of(context).size.height * widget.setWH,
                  height: MediaQuery.of(context).size.height * widget.setWH,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    onPressed: widget.handlePress,
                  ),
                ),
                width: MediaQuery.of(context).size.height * widget.setWH,
                height: MediaQuery.of(context).size.height * widget.setWH,
                decoration: BoxDecoration(
                    color: widget.color ?? Colors.grey,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    image: DecorationImage(
                        image: new NetworkImage(snap
                            .data.snapshot.value[userid1]['profilepicurl']))),
                margin: EdgeInsets.only(
                    right:
                        MediaQuery.of(context).size.width * widget.setMargin),
              );
            }
          } else {
            return Container(
              width: MediaQuery.of(context).size.height * 0.08,
              height: MediaQuery.of(context).size.height * 0.08,
              decoration: BoxDecoration(
                color: widget.color ?? Colors.grey,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * widget.setMargin),
              child: Center(
                child: Container(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }
        });
  }
}

class BottomIcon extends StatelessWidget {
  final String path;
  final select;
  final index;
  bool selected;
  BottomIcon(this.path, this.select, this.index, this.selected);

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) => Container(
        margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
        width: MediaQuery.of(context).size.width * (selected ? 0.18 : 0.15),
        height: MediaQuery.of(context).size.width * (selected ? 0.18 : 0.15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0),
        ),
        child: Material(
          borderRadius: BorderRadius.circular(50),
          color: Colors.transparent,
          child: ButtonTheme(
            minWidth: MediaQuery.of(context).size.width * 0.6,
            height: MediaQuery.of(context).size.width * 0.6,
            shape: CircleBorder(),
            child: FlatButton(
              child: Opacity(
                  opacity: selected ? 1 : 0.5, child: SvgPicture.asset(path)),
              onPressed: () {
                select(index);
              },
            ),
          ),
        ),
      ),
    );
  }
}

class TrendingView extends StatefulWidget {
  @override
  _TrendingViewState createState() => _TrendingViewState();
}

class _TrendingViewState extends State<TrendingView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(
          MediaQuery.of(context).size.width * 0.08,
          MediaQuery.of(context).size.width * 0.04,
          MediaQuery.of(context).size.width * 0.08,
          MediaQuery.of(context).size.width * 0.08),
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (context, index) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.grey[200],
            ),
            margin: EdgeInsets.only(right: 10),
            width: MediaQuery.of(context).size.height * 0.32,
            child: Center(
              child: Text('Placeholder'),
            )),
      ),
    );
  }
}

class RecentView extends StatefulWidget {
  final userid;
  RecentView({this.userid});
  @override
  _RecentViewState createState() => _RecentViewState();
}

class _RecentViewState extends State<RecentView> {
  List projectId = [];
  List<Widget> preview = [];
  var counter = 0;
  Widget view(id) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.95,
        margin: EdgeInsets.only(
            top: 10,
            left: MediaQuery.of(context).size.width * 0.05,
            right: MediaQuery.of(context).size.width * 0.05),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.grey[200],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 4.0, left: 15.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 250,
                    child: StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var chipIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                chipIndex = i;
                              }
                            }
                            if (projectId.isEmpty == false) {
                              if (snap.data.snapshot.value[chipIndex]['tags'] != null) {
                                return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: snap.data.snapshot
                                        .value[chipIndex]['tags'].length,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: EdgeInsets.only(right: 6),
                                        child: Chip(
                                          label: Text(
                                              snap.data.snapshot.value[chipIndex]
                                              ['tags'][index],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12)),
                                          backgroundColor:
                                          Color.fromRGBO(0, 255, 156, 1),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(6.0))),
                                        ),
                                      );
                                    });
                              } else {
                                return Padding(padding: EdgeInsets.only(top: 10));
                              }
                            } else {
                              return Padding(padding: EdgeInsets.only(top: 10));
                            }
                          } else {
                            return Padding(padding: EdgeInsets.only(top: 10));
                          }
                        }),
                  ),
                  StreamBuilder(
                      stream: databaseReference.onValue,
                      builder: (context, snap) {
                        if (snap.hasData &&
                            !snap.hasError &&
                            snap.data.snapshot.value != null) {
                          var picIndex;
                          var picurl;
                          for (var i = 0;
                              i <
                                  snap.data.snapshot.value['projectdatas']
                                      .length;
                              i++) {
                            if (snap.data.snapshot.value['projectdatas'][i]
                                    ['id'] ==
                                id) {
                              picIndex = i;
                            }
                          }
                          if (snap.data.snapshot.value['userdatas'][
                                  snap.data.snapshot.value['projectdatas']
                                      [picIndex]['host']]['profilepicurl'] !=
                              '') {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(snap
                                          .data.snapshot.value['userdatas'][snap
                                              .data
                                              .snapshot
                                              .value['projectdatas'][picIndex]
                                          ['host']]['profilepicurl'])),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0))),
                            );
                          } else {
                            return Container(
                              margin: EdgeInsets.only(right: 15),
                              height: 27,
                              width: 27,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/profpic.png')),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0))),
                            );
                          }
                        } else {
                          return Container(
                            margin: EdgeInsets.only(right: 15),
                            height: 27,
                            width: 27,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/profpic.png')),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6.0))),
                          );
                        }
                      })
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var ideaIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                ideaIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[ideaIndex]['name'],
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          } else {
                            return Text('',
                                style: TextStyle(
                                    fontSize: 26,
                                    fontFamily: 'Neue',
                                    fontWeight: FontWeight.bold));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 5),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream:
                            databaseReference.child('projectdatas/').onValue,
                        builder: (context, snap) {
                          if (snap.hasData &&
                              !snap.hasError &&
                              snap.data.snapshot.value != null) {
                            var descIndex;
                            for (var i = 0;
                                i < snap.data.snapshot.value.length;
                                i++) {
                              if (snap.data.snapshot.value[i]['id'] == id) {
                                descIndex = i;
                              }
                            }
                            return Text(
                                snap.data.snapshot.value[descIndex]['desc'],
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          } else {
                            return Text('',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: 'Neue',
                                    color: Colors.grey[400]));
                          }
                        }),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, top: 18, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Likes(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                        Comments(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                        Accepts(
                          auth: Auth(),
                          id: id,
                          view: 'Column',
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 22),
                      child: StreamBuilder(
                          stream:
                              databaseReference.child('projectdatas/').onValue,
                          builder: (context, snap) {
                            if (snap.hasData &&
                                !snap.hasError &&
                                snap.data.snapshot.value != null) {
                              var idIndex;
                              for (var i = 0;
                                  i < snap.data.snapshot.value.length;
                                  i++) {
                                if (snap.data.snapshot.value[i]['id'] == id) {
                                  idIndex = i;
                                }
                              }
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return ProjectsView(
                                      id: idIndex,
                                      userid: widget.userid,
                                      projectid: id,
                                    );
                                  }));
                                },
                              );
                            } else {
                              return FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                color: Color.fromRGBO(255, 179, 71, 1),
                                child: Text(
                                  'MORE',
                                  style: TextStyle(
                                      letterSpacing: 2.0,
                                      fontSize: 12.0,
                                      fontFamily: 'Neue',
                                      color: Colors.white),
                                ),
                                onPressed: null,
                              );
                            }
                          }),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  buildView() {
    if (counter == 0) {
      counter++;
      projectDataReferenceRecent.once().then((DataSnapshot snapshot) {
        setState(() {
          if (snapshot.value.length <= 5) {
            for (var l = 0; l <= snapshot.value.length - 1; l++) {
              preview.add(view(snapshot.value[l]['id']));
              projectId.add(snapshot.value[l]['id']);
            }
          } else {
            for (var l = snapshot.value.length - 5;
                l <= snapshot.value.length - 1;
                l++) {
              preview.add(view(snapshot.value[l]['id']));
              projectId.add(snapshot.value[l]['id']);
            }
          }
        });
      });
    }
  }

  addTitle() {
    counter++;
    preview.insert(
        0,
        Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.06),
              child: Text(
                'Recent Ideas',
                style: TextStyle(
                    fontSize: 17.0,
                    fontFamily: 'Neue',
                    fontWeight: FontWeight.w100),
              ),
            )));
  }

  @override
  Widget build(BuildContext context) {
    buildView();
    counter == 1 ? addTitle() : null;
    return Column(mainAxisSize: MainAxisSize.min, children: preview);
  }
}

class Likes extends StatelessWidget {
  final BaseAuth auth;
  final view;
  final id;
  Likes({this.auth, this.id, this.view});

  show() {
    switch (view) {
      case 'Column':
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/likeOutlineBlack.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas/').onValue,
                builder: (context, likesnap) {
                  if (likesnap.hasData &&
                      !likesnap.hasError &&
                      likesnap.data.snapshot.value != null) {
                    var likeIndex;
                    for (var i = 0;
                        i < likesnap.data.snapshot.value.length;
                        i++) {
                      if (likesnap.data.snapshot.value[i]['id'] == id) {
                        likeIndex = i;
                      }
                    }
                    return Container(
                      child: Text(
                          likesnap
                              .data.snapshot.value[likeIndex]['upvotes'].length
                              .toString(),
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
      case 'Row':
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5),
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/likeOutlineBlack.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas/').onValue,
                builder: (context, likesnap) {
                  if (likesnap.hasData &&
                      !likesnap.hasError &&
                      likesnap.data.snapshot.value != null) {
                    if (likesnap.data.snapshot.value[id]['upvotes'].length !=
                        0) {
                      return Container(
                        child: Text(
                            likesnap.data.snapshot.value[id]['upvotes'].length
                                .toString(),
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    } else {
                      return Container(
                        child: Text('0',
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    }
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: show(),
    );
  }
}

class Comments extends StatelessWidget {
  final BaseAuth auth;
  final view;
  final id;
  Comments({this.auth, this.id, this.view});
  show() {
    switch (view) {
      case 'Column':
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/commentBlack.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas').onValue,
                builder: (context, commentsnap) {
                  if (commentsnap.hasData &&
                      !commentsnap.hasError &&
                      commentsnap.data.snapshot.value != null) {
                    var commentIndex;
                    for (var i = 0;
                        i < commentsnap.data.snapshot.value.length;
                        i++) {
                      if (commentsnap.data.snapshot.value[i]['id'] == id) {
                        commentIndex = i;
                      }
                    }
                    return Container(
                      child: Text(
                          commentsnap.data.snapshot
                              .value[commentIndex]['comments'].length
                              .toString(),
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
      case 'Row':
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5),
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/commentBlack.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas').onValue,
                builder: (context, commentsnap) {
                  if (commentsnap.hasData &&
                      !commentsnap.hasError &&
                      commentsnap.data.snapshot.value != null) {
                    if (commentsnap
                            .data.snapshot.value[id]['comments'].length !=
                        0) {
                      return Container(
                        child: Text(
                            commentsnap
                                .data.snapshot.value[id]['comments'].length
                                .toString(),
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    } else {
                      return Container(
                        child: Text('0',
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    }
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: show(),
    );
  }
}

class Accepts extends StatelessWidget {
  final BaseAuth auth;
  final view;
  final id;
  Accepts({this.auth, this.id, this.view});

  show() {
    switch (view) {
      case 'Column':
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/accept.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas').onValue,
                builder: (context, acceptsnap) {
                  if (acceptsnap.hasData &&
                      !acceptsnap.hasError &&
                      acceptsnap.data.snapshot.value != null) {
                    var acceptIndex;
                    for (var i = 0;
                        i < acceptsnap.data.snapshot.value.length;
                        i++) {
                      if (acceptsnap.data.snapshot.value[i]['id'] == id) {
                        acceptIndex = i;
                      }
                    }
                    return Container(
                      child: Text(
                          acceptsnap.data.snapshot
                              .value[acceptIndex]['involved'].length
                              .toString(),
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
      case 'Row':
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5),
              height: 20,
              width: 20,
              child: SvgPicture.asset('assets/icons/accept.svg'),
            ),
            StreamBuilder(
                stream: databaseReference.child('projectdatas').onValue,
                builder: (context, acceptsnap) {
                  if (acceptsnap.hasData &&
                      !acceptsnap.hasError &&
                      acceptsnap.data.snapshot.value != null) {
                    if (acceptsnap.data.snapshot.value[id]['involved'].length !=
                        0) {
                      return Container(
                        child: Text(
                            acceptsnap
                                .data.snapshot.value[id]['involved'].length
                                .toString(),
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    } else {
                      return Container(
                        child: Text('0',
                            style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                      );
                    }
                  } else {
                    return Container(
                      child: Text('',
                          style: TextStyle(fontFamily: 'Neue', fontSize: 12)),
                    );
                  }
                })
          ],
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 12),
      child: show(),
    );
  }
}
