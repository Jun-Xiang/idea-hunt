# idea_hunt

IdeaHunt App, built with Flutter on the Android Open Source Platform.

## Introduction

Targeting Goal 8: Decent Work and Economic Growth in the [United Nations Sustainable Development Goals](https://www.un.org/sustainabledevelopment/sustainable-development-goals/), our app aims to help people like graphics designers, app developers and web developers(for now) to find suitable jobs in their career. Our app works as stated below: 

1. A person came up with an idea on developing something and they want to find people from all around the world to help him/her on this. 
   AND
   A person graduated from university but can't get a job, he/she wants to find ideas to startup his career.

2. User uses IdeaHunt app to submit their new ideas and develop his/her ideas with other people. 
   AND 
   User uses IdeaHunt app to find new ideas to work on and develop together with other professionals.

3. In the end, both of them are able to work on their startup together and gain profit (a win-win situation here!).

## Conclusion

IdeaHunt App provides a platform to share/discuss ideas and help people to find their next career oppurtunity.

## App Info

This app is built with Flutter in Android Studio, an open-source mobile application development framework created by Google. It is used to develop applications for Android and iOS, as well as being the primary method of creating applications for Google Fuchsia.

This app uses Google Firebase Authentication to enable users to sign up or login their account whenever they are and Realtime Database for storing every datas created in the app. 

Miminum Device Requirements:

| Android OS Version                | Network Condition   |
| ----------------------------------|:-------------------:|
| Android Marshmallow(6.0)(API 23)  | 2G                  |

## Developers
1. Ooi Jun Xiang [Jun-Xiang](https://gitlab.com/Jun-Xiang)
2. Cheah Zixu [genesis331](https://gitlab.com/genesis331)

## Contributing
This project does not support any contributions yet.